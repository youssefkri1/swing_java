package com.rattrapageswing.component;

import java.util.List;

public interface Phase {

     void selectPlayer(List<Player> players);

     void gamePhase();
}
