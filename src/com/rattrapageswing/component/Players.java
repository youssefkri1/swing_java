package com.rattrapageswing.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Players {

    Player[] players = new Player[20];

    Players(Player[] players){
        this.players = players;
    }

    Players(){}

    Players(boolean init){

        this.init();
    }
    public void init(){
        this.players[0] = new Player("A");
        this.players[1] = new Player("B");
        this.players[2] = new Player("C");
        this.players[3] = new Player("D");
        this.players[4] = new Player("E");
        this.players[5] = new Player("F");
        this.players[6] = new Player("G");
        this.players[7] = new Player("H");
        this.players[8] = new Player("I");
        this.players[9] = new Player("J");
        this.players[10] = new Player("K");
        this.players[11] = new Player("L");
        this.players[12] = new Player("M");
        this.players[13] = new Player("N");
        this.players[14] = new Player("O");
        this.players[15] = new Player("P");
        this.players[16] = new Player("Q");
        this.players[17] = new Player("R");
        this.players[18] = new Player("S");
        this.players[19] = new Player("T");
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public Player[] selectNmbPlayers(int nmb){
        //Player[] oldPlayers = this.players;
        Player[] players = new Player[nmb];
        List<Player> oldPlayers = new ArrayList<Player>(Arrays.stream(this.players).toList());
        int length = 19;
        for(int i=0; i<nmb; i++){
            int rnd = new Random().nextInt(length);
            //var v = oldPlayers.get(rnd);
            players[i] = oldPlayers.get(rnd);
            oldPlayers.remove(rnd);
            length --;
        }
        return players;
    }

    public Player selectPlayer(){
        int rnd = new Random().nextInt(19);
        return this.players[rnd];
    }

    public Player[] selectWinners(int nbWinners){
        Player min = players[0];
        int indiceMin = 0;
        Player[] result = new Player[nbWinners];
        for(int i=1; i<players.length; i++) {
            if (players[i].getScore() < min.getScore()) {
                min = players[i];
                indiceMin = i;
            }
        }

        int j =0;
        for(int i = 0; i<players.length; i++){
            if(i != indiceMin){
                result[j] = players[i];
                j++;
            }
        }
        return result;
    }

    public String show(){
        String result = "Les joueurs sont : \n ";
        for(Player player : this.players){
            result+=  ("joueur numero " + player.getNum() +" , sont nom est : " + player.getName() + "  ,  il est : " + player.getState() + " ,son score est : " + player.getScore() + "\n");
        }
        return result;
    }
}
