package com.rattrapageswing.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ThemeList {

    private List<Theme> themes;
    private Theme selectedTheme;

    ThemeList(boolean init) {
        this.themes = initThemes();
    }
    ThemeList() {
    }

    public List<Theme> getThemes(){
        return this.themes;
    }

    public Theme getSelectedTheme() {
        return selectedTheme;
    }

    private List<Theme> initThemes() {
        List<Theme> themes = new ArrayList<Theme>( );
        themes.add(new Theme("Sport"));
        themes.add(new Theme("Histoire"));
        themes.add(new Theme("Culture"));
        themes.add(new Theme("Science"));
        themes.add(new Theme("musique"));
        themes.add(new Theme("cinéma"));
        themes.add(new Theme("Enigme"));
        themes.add(new Theme("Geographie"));
        themes.add(new Theme("Anglais"));
        themes.add(new Theme("Calcul"));

        return themes;
    }

    private Theme SelectTheme(){

        List<Theme> ths = this.themes.stream().filter(th -> th.getIsSelected() == false).collect(Collectors.toList());
        int randomIndice = new Random().nextInt(ths.size());

        return ths.get(randomIndice);

    }

    public void modifyTheme(Theme theme){
        if(theme.getIsSelected()){
            // on ne peut pas le sélectionner
            System.out.println("thème déjà pris");
        }else {
            theme.setIsSelected(true);
            this.selectedTheme = theme;
        }
    }

    public List<Theme> selectNumberThemes(int number){
        List<Theme> ths = this.themes.stream().filter(th -> th.getIsSelected() == false).collect(Collectors.toList());
        List<Theme> th = new ArrayList<Theme>();
        for(int i=0; i<number; i++){
            int randomIndice = new Random().nextInt(ths.size());
            th.add(ths.get(randomIndice));
            ths.remove(randomIndice);
        }
        return th;
    }

    private String show(){
        return "";
    }

    @Override
    public String toString() {
        return "ThemeList{" +
                "themes=" + themes +
                ", selectedTheme=" + selectedTheme +
                '}';
    }
}


