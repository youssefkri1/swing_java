package com.rattrapageswing.component;

public class Statement {
    private String question;
    private TypeQuestion typeQuestion;

    Statement(String question, TypeQuestion typeQuestion){
        this.question = question;
        this.typeQuestion = typeQuestion;
       /* switch (typeQuestion) {
            case "VF" -> this.typeQuestion = TypeQuestion.VF;
            case "RC" -> this.typeQuestion = TypeQuestion.RC;
            case "QCM" -> this.typeQuestion = TypeQuestion.QCM;
            default -> {
            }
        }
        */

        /*if(typeQuestion == "VF"){
            this.typeQuestion = TypeQuestion.VF;
        }else if(typeQuestion == "RC"){
            this.typeQuestion = TypeQuestion.RC;
        } else if(typeQuestion == "QCM"){
            this.typeQuestion = TypeQuestion.QCM;
        }*/
    }

    public String getQuestion() {
        return question;
    }

    public TypeQuestion getTypeQuestion() {
        return typeQuestion;
    }
}
