package com.rattrapageswing.component;

public class QCMQuestion extends  Question {

    private String[] responses = new String[3];
    private int correctResponse; //1 si première, 2 si 2eme et 3 si 3eme
    QCMQuestion(Statement statement, Theme theme, int difficultyLevel, String[] responses,  int correctResponse){
        super(statement, theme, difficultyLevel);
        this.responses = responses;
        this.correctResponse = correctResponse;
    }

    public int getCorrectResponse() {
        return correctResponse;
    }

    @Override
    public String afficherQuestion() {
        String result = this.statement.getQuestion() + "\n";
        int i =1;
        for(String res : responses){
            result+= " "+i + ") "+ res + "\n";
            i++;
        }
        return result;
    }

    public String[] getResponses() {
        return responses;
    }

    public boolean saisir(int response){
        if(this.correctResponse == response){
            return true;
        }
        return false;
    }
}
