package com.rattrapageswing.component;

import java.util.List;

public class ThemeQuestions {
    private Theme theme;
    private List<Question> questions;

    ThemeQuestions(){
    }

    ThemeQuestions(Theme theme, List<Question> questions){
        this.theme = theme;
        this.questions = questions;
    }


    public Theme getTheme(){
        return this.theme;
    }

    public List<Question> getQuestions(){
        return this.questions;
    }

    public void addQuestion(Question question){
        //System.out.println(questions.get(0).getStatement().getQuestion());
        this.questions.add(question);
    }

    public void deleteQuestion(Question question){
        this.questions.remove(question);
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void selectionnerQuestion(Player player, Question question){

    }

    public String show(){
        String result = "";
        int i=1;
        for(Question q : questions){
            result += ("La" + i + "ème question du thème " + theme.getName()+ " est : " + q.getStatement().getQuestion() +"est de type : " +q.getStatement().getTypeQuestion() + "\n");
        i++;
        }
        return result;
    }
}
