package com.rattrapageswing.component;

public enum PlayerState {
    SELECTED,
    WINNER,
    SUPER_WINNER,
    ELIMINATED,
    WAITING
}
