package com.rattrapageswing.component;

public abstract class Question<TypeQuestion> implements QuestionInterface{

    // l'énoncé
    protected Statement statement;

    private static int count = 0;;
    private int number;
    private Theme theme;
    private int difficultyLevel;

    Question(Statement statement, Theme theme, int difficultyLevel){
        this.statement = statement;
        this.theme = theme;
        this.difficultyLevel = difficultyLevel;
        this.setNumber(++count);


    }

    public Statement getStatement() {
        return statement;
    }

    public int getDifficultyLevel() {
        return difficultyLevel;
    }

    public int getNumber() {
        return number;
    }

    public Question() {
        this.setNumber(++count);
    }

    public void setNumber(int number){
        this.number = number;
    }

    public abstract String afficherQuestion(

    );

}
