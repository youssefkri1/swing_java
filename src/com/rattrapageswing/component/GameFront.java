package com.rattrapageswing.component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GameFront extends JFrame {

    //init Themes and initQuestions

    private GameService gameService;

    Player[] selectedPlayers;
    List<Theme> availableThemes;

    private Game game;

    private JPanel buttonPanel, labelPanel,buttonPanel2, buttonPanel3,  themePanel, hlabelPanel, qbPanel1, qbPanel2;
    private JButton button, button1, button2, button3, button4, button5, button6,   hbutton1, hbutton2, hbutton3;
    private String[] all_lines;
    private int limit = 0, counter = 1, idx = 0, score = 0;
    private JFrame qFrame, rFrame, tFrame, sFrame;
    private String s;
    private Path currentPath, question;

    public static void main(String[] args){
        GameFront gameWindow = new GameFront();
        gameWindow.setVisible(true);
    }

    GameFront(){
        super("Quiz rattrapage java");
        ThemeList themes = new ThemeList();
        Questions questions = new Questions();
        Players players = new Players();
        this.gameService = new GameService(themes, questions, players);
        gameService.initGame();


        setLayout(new GridLayout(6, 1));
        setBounds(448, 500, 500, 1500);
        //showListThemeQuestions();

        labelPanel = new JPanel();
        labelPanel.add(new JLabel("Ajouter / Supprimer des questions?"));
        buttonPanel = new JPanel(new FlowLayout());

        // call createButton function
        button1 = CreateButton("Ajouter Question RC", "Ajouter une question dans un thème, avec un niveau de difficulté");
        button2 = CreateButton("Ajouter Question VF", "Ajouter une question dans un thème, avec un niveau de difficulté");
        button3 = CreateButton("Ajouter Question QCM", "Ajouter une question dans un thème, avec un niveau de difficulté");

        //button2 = CreateButton("20(Medium)", "Click this to answer 20 question");
        //button3 = CreateButton("30(Hard)", "Click this to answer 30 question");

        buttonPanel.add(button1); buttonPanel.add(button2); buttonPanel.add(button3);



        add(labelPanel);
        add(buttonPanel);

        buttonPanel3 = new JPanel((new FlowLayout()));
        button5 = CreateButton("Afficher la liste de thème questions", "Chaque thème contient des questions");
        button6 = CreateButton("Afficher la liste des joueurs", "Les joueurs de A à J (10)");
        buttonPanel3.add(button5); buttonPanel3.add(button6);
        add(buttonPanel3);


        buttonPanel2 = new JPanel(new FlowLayout());
        button4 = CreateButton("Commencer le jeu", "On choisis 4 joueurs aléatoirement");
        buttonPanel2.add(button4);
        add(buttonPanel2);



        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);

        // call getData(null)
        //getData();

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button1) {
                    addRCQuestion();
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button2) {
                    addVFQuestion();
                }
            }
        });

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button3) {
                    addQCMQuestion();
                }
            }
        });

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button4) {
                    initGame();
                }
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button5) {
                    showListThemeQuestions();
                }
            }
        });

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(actionEvent.getSource() == button6) {
                    showListPlayers();
                }
            }
        });
    }

    public JButton CreateButton(String label, String toolTipText) {
        button = new JButton(label);
        button.setToolTipText(toolTipText);
        button.setBorderPainted(false);

        ButtonHover(button);

        return button;
    }

    public void ButtonHover(JButton button) {
        button.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                button.setBackground(Color.GREEN);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                button.setBackground(UIManager.getColor("control"));
            }
        });
    }

    public void addRCQuestion(){

        // la liste des themes
        String[] themes = {"Sport", "Histoire", "Culture", "Science", "Musique", "Cinéma", "Enigme", "Géographie", "Anglais", "Calcul"};
        JComboBox comboThemes = new JComboBox(themes);
        JTextField question = new JTextField(10);
        String[] difficultylevel = {"1", "2", "3"};
        JComboBox comboDifficultyLevel = new JComboBox(difficultylevel);
        JTextField response = new JTextField(10);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Thème:"));
        panel.add(comboThemes);
        panel.add(new JLabel("Question :"));
        panel.add(question);
        panel.add(new JLabel("Niveau de difficulté :"));
        panel.add(comboDifficultyLevel);
        panel.add(new JLabel("Réponse correcte :"));
        panel.add(response);


        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            this.gameService.addRCQuestion(comboThemes.getSelectedIndex()+1, question.getText(), comboDifficultyLevel.getSelectedIndex()+1, response.getText());
        } else {
            System.out.println("Cancelled");
        }


    }

    public void addVFQuestion(){

        // la liste des themes
        String[] themes = {"Sport", "Histoire", "Culture", "Science", "Musique", "Cinéma", "Enigme", "Géographie", "Anglais", "Calcul"};
        JComboBox comboThemes = new JComboBox(themes);
        JTextField question = new JTextField(10);
        String[] difficultylevel = {"1", "2", "3"};
        JComboBox comboDifficultyLevel = new JComboBox(difficultylevel);
        String[] response = {"Vrai", "Faux"};
        JComboBox responseCombo = new JComboBox(response);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Thème:"));
        panel.add(comboThemes);
        panel.add(new JLabel("Question :"));
        panel.add(question);
        panel.add(new JLabel("Niveau de difficulté :"));
        panel.add(comboDifficultyLevel);
        panel.add(new JLabel("Réponse correcte :"));
        panel.add(responseCombo);


        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            boolean vf = (responseCombo.getSelectedIndex()==0)? true: false;
            this.gameService.addVFQuestion(comboThemes.getSelectedIndex()+1, question.getText(), comboDifficultyLevel.getSelectedIndex()+1, vf);
        } else {
            System.out.println("Cancelled");
        }


    }

    public void addQCMQuestion(){

        // la liste des themes
        String[] themes = {"Sport", "Histoire", "Culture", "Science", "Musique", "Cinéma", "Enigme", "Géographie", "Anglais", "Calcul"};
        JComboBox comboThemes = new JComboBox(themes);
        JTextField question = new JTextField(10);
        JTextField response1 = new JTextField(10);
        JTextField response2 = new JTextField(10);
        JTextField response3 = new JTextField(10);
        String[] difficultylevel = {"1", "2", "3"};
        JComboBox comboDifficultyLevel = new JComboBox(difficultylevel);
        String[] correctResponseProposition = {"1", "2", "3"};
        JComboBox correctResponseCombo = new JComboBox(correctResponseProposition);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Thème:"));
        panel.add(comboThemes);
        panel.add(new JLabel("Question :"));
        panel.add(question);
        panel.add(new JLabel("Réponse 1 :"));
        panel.add(response1);
        panel.add(new JLabel("Réponse 2 :"));
        panel.add(response2);
        panel.add(new JLabel("Réponse3 :"));
        panel.add(response3);
        panel.add(new JLabel("Niveau de difficulté :"));
        panel.add(comboDifficultyLevel);
        panel.add(new JLabel("Réponse correcte :"));
        panel.add(correctResponseCombo);


        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            this.gameService.addQCMQuestion(comboThemes.getSelectedIndex()+1, question.getText(), comboDifficultyLevel.getSelectedIndex()+1, new String[]{response1.getText(), response2.getText(), response3.getText()}, correctResponseCombo.getSelectedIndex()+1);
        } else {
            System.out.println("Cancelled");
        }


    }

    public void showListThemeQuestions(){
        JPanel themeQuestionPanel = new JPanel(new GridLayout(0, 1));
        for(ThemeQuestions themeQuestion : gameService.themeQuestionsList){
            themeQuestionPanel.add(new JLabel("Pour le thème : " + themeQuestion.getTheme().getName() + " ,les questions sont"));
            int i = 1;
            for(Question question : themeQuestion.getQuestions()){
                String q= "   "+i+")" + question.getStatement().getQuestion() + "\n";
                i++;
                themeQuestionPanel.add(new JLabel(q));
                //revenir au panel principal
            }

            }

        int result = JOptionPane.showConfirmDialog(null, themeQuestionPanel, "Liste de Thème Questions",JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            // revenir à notre pannel
        }
    }

    public void showListPlayers(){
        JPanel playersPanel = new JPanel(new GridLayout(0, 1));
        for(Player player : gameService.players.getPlayers()){
            playersPanel.add(new JLabel("Le joueur  : " + player.getName() + " , son état est " + player.getState().toString()));
        }

        int result = JOptionPane.showConfirmDialog(null, playersPanel, "Liste de joueurs",JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            // revenir à notre pannel
        }
    }

    public void initGame(){
        JPanel playersPanel = new JPanel(new GridLayout(0, 1));
        selectedPlayers = gameService.selectNmbPlayers(4);

        String sp = "Les 4 sjoueurs sélectionnés pour cette partie sont : ";
        for(Player player : selectedPlayers){
            sp+= ", " +player.getName();
        }
        playersPanel.add(new JLabel(sp));
        playersPanel.add(new JLabel("Les joueurs séléctionnées sont prets ? le jeu commencera dans 5 secondes"));
        int result = JOptionPane.showConfirmDialog(null, playersPanel, "Liste de joueurs",JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            // revenir à notre pannel
            startPhase1();
        }
    }

    public void startPhase1(){
        System.out.println("rentré dans la phase 1");
        Theme selectedTheme = this.gameService.themeQuestionsList.get(0).getTheme();
        List<Question> askedQuestions = this.gameService.themeQuestionsList.get(0).getQuestions();

        //we need the copy to not damage the real themes
        List<Question> finalAskedQuestions = new ArrayList<Question>(askedQuestions.stream().filter(q -> q.getDifficultyLevel() == 1).collect(Collectors.toList()));
        Stream<Player> intParallelStream= Arrays.stream(selectedPlayers).parallel();
        System.out.println("déclarer dans la phase 1 les paralle");
        displayScoring(selectedPlayers, "1", false);

        intParallelStream.forEach(player->
        {
            System.out.println("avant de commencer la phase per player");
            phasePerPlayer("1",selectedPlayers, player, new ArrayList<Theme>(){
                {add(selectedTheme);
                }}, finalAskedQuestions, 2);
        });

        startPhase2();

    }

    public void displayScoring(Player[]players, String phase, boolean finish){
        qFrame = new JFrame("Phase :  " + phase );
        qFrame.setBounds(157,235, 500, 200);
        qFrame.setLayout(new GridLayout(3,1));


        labelPanel = new JPanel(new GridLayout(0, 1));
        if(!finish) {
        labelPanel.add(new JLabel("Phase : " + phase));
            for (Player player : players) {
                labelPanel.add(new JLabel(" : Joueur " + player.getName() + " : " + String.valueOf(player.getScore())));

            }
        }else {
            labelPanel.add(new JLabel("Jeu fini"));
            labelPanel.add(new JLabel("félicitation au vainqueur :" + players[0].getName() ));
        }

        qFrame.add(labelPanel);
        qFrame.pack();
        qFrame.setVisible(true);
    }

    public void phasePerPlayer(String phase, Player[] players, Player player,List<Theme> themes,  List<Question> askedQuestions, int point){



        rFrame = new JFrame("Phase : 1 "  + " : Joueur " + player.getName() + " , phase : " + phase + " , score:  : " + String.valueOf(player.getScore()));

        rFrame.setPreferredSize(new Dimension(640, 480));
        rFrame.setLayout(new GridLayout(3,1));
        for(Question question : askedQuestions) {
            responde(players, phase, player, themes, question, point);
        }


    }



    public void responde(Player[] players, String phase, Player player,List<Theme> themes,  Question askedQuestions, int point){
        switch(askedQuestions.getStatement().getTypeQuestion()){
            case QCM ->  rFrame.add(askQCMQuestion(players, phase, player, themes, askedQuestions, point));
            case VF ->   rFrame.add(askVFQuestion( players, phase, player, themes,  askedQuestions, point));
            case RC ->   rFrame.add(askRCQuestion( players, phase, player, themes, askedQuestions, point));


        }
    }

    public void startPhase2(){
        System.out.println("la 2ème phase va commencer bientôt");
        Player[] winnersOfPhase1 = gameService.selectWinners(selectedPlayers,3);
        qFrame.dispose();
        displayScoring(winnersOfPhase1,"2", false);
        System.out.println(winnersOfPhase1.length);
        availableThemes =  new ArrayList<Theme>(gameService.themes.getThemes());
        HashMap<Player, List<Theme>> themesPerPlayer = new HashMap<Player, List<Theme>>();
        for(Player player: winnersOfPhase1){
            List<Theme> twoThemesPerPlayer = choose2Themes(player);
            themesPerPlayer.put(player, twoThemesPerPlayer);
        }
        Stream<Player> intParallelStream= Arrays.stream(winnersOfPhase1).parallel();
        intParallelStream.forEach(player->
        {
            List<Question> askedQuestions =   new ArrayList<Question>(this.gameService.themeQuestionsList.stream().filter(tq-> tq.getTheme() == themesPerPlayer.get(player).get(0)).collect(Collectors.toList()).get(0).getQuestions());
            askedQuestions.addAll(this.gameService.themeQuestionsList.stream().filter(tq-> tq.getTheme() == themesPerPlayer.get(player).get(1)).collect(Collectors.toList()).get(0).getQuestions());
            List<Question> finalAskedQuestions = new ArrayList<Question>(askedQuestions.stream().filter(q -> q.getDifficultyLevel() == 2).collect(Collectors.toList()));
            System.out.println("for player "+ player + "il a : " +askedQuestions.size() + "questions");
            phasePerPlayer("2", winnersOfPhase1, player, themesPerPlayer.get(player), finalAskedQuestions, 3);
        });

        startPhase3(winnersOfPhase1);
    }

    public void startPhase3(Player[] winnersOfPhase1) {
        Player[] winnersOfPhase2 = gameService.selectWinners(winnersOfPhase1, 2);
        qFrame.dispose();
        displayScoring(winnersOfPhase2, "3", false);
        List<Theme> themesPhase3 = gameService.themes.selectNumberThemes(3);
        List<Question> askedQuestions =  new ArrayList<Question>( this.gameService.themeQuestionsList.stream().filter(tq-> tq.getTheme() == themesPhase3.get(0)).collect(Collectors.toList()).get(0).getQuestions());
        askedQuestions.addAll(this.gameService.themeQuestionsList.stream().filter(tq-> tq.getTheme() == themesPhase3.get(1)).collect(Collectors.toList()).get(0).getQuestions());
        askedQuestions.addAll(this.gameService.themeQuestionsList.stream().filter(tq-> tq.getTheme() == themesPhase3.get(2)).collect(Collectors.toList()).get(0).getQuestions());
        List<Question> finalAskedQuestions =  new ArrayList<Question>(askedQuestions.stream().filter(q -> q.getDifficultyLevel() == 3).collect(Collectors.toList()));
        System.out.println("la taille des questions est : " + askedQuestions.size());
        Stream<Player> intParallelStream= Arrays.stream(winnersOfPhase2).parallel();
        intParallelStream.forEach(player->
        {
            phasePerPlayer("3", winnersOfPhase2, player, themesPhase3, finalAskedQuestions, 5);
        });

       declareWinner(winnersOfPhase2);
    }



    public JPanel askVFQuestion( Player[] players, String phase ,Player player, List<Theme> themes, Question askedQuestion, int point){
        JPanel vf = new JPanel(new GridLayout(0, 1));
        vf.setPreferredSize(new Dimension(640, 480));
        vf.add(new JLabel(askedQuestion.getStatement().getQuestion()));
        JComboBox combo = new JComboBox(new String[]{"vrai", "faux"});
        vf.add(combo);
        String thString = "Themes : ";
        for(Theme theme : themes){
            thString += theme.getName() + " ";
        }
        int result = JOptionPane.showConfirmDialog(null, vf, "Phase "+phase + "Themes : " + thString +" : Joueur " + player.getName() + ", score " + player.getScore(),
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            boolean response = (combo.getSelectedIndex()==0)? true: false;
            player.saisir((VFQuestion) askedQuestion, response, point );
            qFrame.dispose();
            displayScoring(players, phase, false);

        } else {
            System.out.println("Cancelled");
        }
        return vf;
    }

    public JPanel askQCMQuestion(Player[] players, String phase ,Player player, List<Theme> themes, Question askedQuestion, int point){
        JPanel qcm = new JPanel(new GridLayout(0, 1));
        qcm.setPreferredSize(new Dimension(640, 480));
        qcm.add(new JLabel(askedQuestion.getStatement().getQuestion()));
        String[] responses = ((QCMQuestion)askedQuestion).getResponses();
        JComboBox combo = new JComboBox(responses);
        qcm.add(combo);
        String thString = "Themes : ";
        for(Theme theme : themes){
            thString += theme.getName() + " ";
        }
        int result = JOptionPane.showConfirmDialog(null, qcm,"Phase "+phase + "Themes : " + thString +" :  Joueur " + player.getName() + ", score " + player.getScore(),
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            player.saisir((QCMQuestion) askedQuestion, combo.getSelectedIndex(), point );
            qFrame.dispose();
            displayScoring(players, phase, false);

        } else {
            System.out.println("Cancelled");
        }
        return qcm;
    }

    public JPanel askRCQuestion(Player[] players, String phase ,Player player, List<Theme> themes, Question askedQuestion, int point){
        JPanel rc = new JPanel(new GridLayout(0, 1));
        rc.setPreferredSize(new Dimension(640, 480));
        rc.add(new JLabel(askedQuestion.getStatement().getQuestion()));
        JTextField res = new JTextField();
        rc.add(res);
        String thString = "Themes : ";
        for(Theme theme : themes){
            thString += theme.getName() + " ";
        }
        int result = JOptionPane.showConfirmDialog(null, rc, "Phase "+phase + "Themes : " + thString + " : Joueur " + player.getName() + ", score " + player.getScore(),
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {

            player.saisir((RCQuestion) askedQuestion, res.getText(), point );
            qFrame.dispose();
            displayScoring(players, phase, false);

        } else {
            System.out.println("Cancelled");
        }
        return rc;

    }

    public List<Theme> choose2Themes(Player player){

        //tFrame = new JFrame("Thème joueur " + player.getName());
        //tFrame.setBounds(157,235, 500, 200);
        //tFrame.setLayout(new GridLayout(3,1));
        JPanel themePanel = new JPanel(new GridLayout(0, 1));
        themePanel.add(new JLabel("Choisissez votre thème: " ));
        for(int i =0; i< availableThemes.size(); i++){
            themePanel.add(new JLabel(String.valueOf( i+1) + ")" + availableThemes.get(i).getName()));
        }
        JTextField choosen1theme = new JTextField("1");
        JTextField choosen2theme = new JTextField("2");
        themePanel.add(choosen1theme);
        themePanel.add(choosen2theme);
        //tFrame.add(themePanel);
        String ct1 = choosen1theme.getText();
        String ct2 = choosen2theme.getText();

        Theme theme1 = availableThemes.get(Integer.parseInt(ct1)-1);
        Theme theme2 = availableThemes.get(Integer.parseInt(ct2)-1);

        List<Theme> result = new ArrayList<Theme>(Arrays.asList(theme1, theme2));
        int res = JOptionPane.showConfirmDialog(null, themePanel, "Joueur " + player.getName(),
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (res == JOptionPane.OK_OPTION) {
        availableThemes.remove(theme1);
        availableThemes.remove(theme2);
            return(result);

        } else {
            return result;
        }

        //tFrame.pack();
        //tFrame.setVisible(true);

        //return(result);
    }

    public void declareWinner(Player[] winnersOfPhase2){
        Player[] winner = gameService.selectWinners(winnersOfPhase2, 1);
        winner[0].setState(PlayerState.SUPER_WINNER);
        System.out.println("Félicitation au vainqueur du jeu " + winner[0].getName());
        qFrame.dispose();
        displayScoring(winner, "3", true);
    }

    public static void setTimeout(Runnable runnable, int delay){
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            }
            catch (Exception e){
                System.err.println(e);
            }
        }).start();
    }
}
