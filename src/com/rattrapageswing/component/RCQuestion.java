package com.rattrapageswing.component;

public class RCQuestion extends Question{

    private String response;

    RCQuestion(Statement statement, Theme theme, int difficultyLevel, String response){
        super(statement, theme, difficultyLevel);
        this.response = response;
    }
    @Override
    public String afficherQuestion() {
        return this.statement.getQuestion();
    }

    public boolean saisir(String response){
        if(new String(response).equals(this.response)){
            return true;
        }
        return false;
    }
}
