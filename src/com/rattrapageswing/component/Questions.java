package com.rattrapageswing.component;

import java.util.List;
import java.util.stream.Collectors;

public class Questions {
    // cette classe est comme ListThemes
    private List<ThemeQuestions> themeQuestions;
    private int iterator;

    Questions(){}

    Questions(List<ThemeQuestions> themeQuestions){
        this.themeQuestions = themeQuestions;
        this.iterator = 0;
    }

    Questions thisQuestion(){
        return this;
    }

    public void addQuestion(Theme theme, Question question){
        this.themeQuestions.stream().filter(th -> th.getTheme()==theme).collect(Collectors.toList()).get(0).addQuestion(question);
    }

    public void deleteQuestion(Theme theme, Question question){
        this.themeQuestions.stream().filter(th -> th.getTheme()==theme).collect(Collectors.toList()).get(0).deleteQuestion(question);
    }

    public String show(){
        String result = "";
        for(ThemeQuestions themeQuestions : themeQuestions){
            result += themeQuestions.show();
        }
        return result;
    }




}
