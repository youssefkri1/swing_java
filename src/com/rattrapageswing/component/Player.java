package com.rattrapageswing.component;

public class Player {

    private String name;
    private static int iter = 90;
    private int num;
    private PlayerState state;
    private int score =0;

    Player(String name){
        this.name = name;
        this.state = PlayerState.WAITING;
        this.setNum(this.iter += 10);
    }

    Player(Player player){
        this.name = player.getName();
        this.state = PlayerState.WAITING;
        this.score = 0;
        this.num += 10;
    }
    public String getName(){
        return this.name;
    }

    public PlayerState getState() {
        return state;
    }


    public int getScore() {
        return score;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }


    public void setScore(int score) {
        this.score = score;
    }



    public void saisir(QCMQuestion question, int response, int point){
        if(question.saisir(response)){
            this.setScore(this.getScore()+point);
        }
    }

    public void saisir(RCQuestion question,  String response, int point){
        if(question.saisir(response)){
            this.setScore(this.getScore()+point);}
    }

    public void saisir(VFQuestion question, boolean response, int point){
        if(question.saisir(response)){
            this.setScore(this.getScore()+point);
        }
    }


    public String show() {
        return "Player{" +
                "name='" + getName() + '\'' +
                '}';
    }
}
