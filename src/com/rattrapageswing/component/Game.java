package com.rattrapageswing.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game implements Phase{
    private GameService gameService;
    private Players players;
    private ThemeList themes;
    private List<ThemeQuestions> themeQuestionsList;
    private Questions questions;




    Game( GameService gameService){
        this.gameService = gameService;
    }

    @Override
    public void selectPlayer(List<Player> players) {

    }

    @Override
    public void gamePhase() {

    }


    //peut-être la méthode renvoie 3 joueurs gagnants
    public Player[] startPhase1(){
        var players1Phase =  new Players(this.players.selectNmbPlayers(4));
        String players_name = "";
        for(Player player : players1Phase.getPlayers()){
            players_name += (player.getName() + " ");
            player.setState(PlayerState.SELECTED);
        }
            System.out.println("Les joueurs séléctionnées sont : " + players_name + "\n");
        //Theme firstthemeSelected = this.themes.getThemes().get(0);
        this.themes.getThemes().get(0).setIsSelected(true);
        List<Question> qs =  this.themeQuestionsList.get(0).getQuestions();

        for(Question q : qs){
            TypeQuestion typeQuestion =  q.getStatement().getTypeQuestion();
        switch (typeQuestion.toString()){
            case "RC" -> gameService.askQuestion(players1Phase.getPlayers(), (RCQuestion) q, 2);
            case "QCM" -> gameService.askQuestion(players1Phase.getPlayers(), (QCMQuestion) q, 2);
            case "VF" -> gameService.askQuestion(players1Phase.getPlayers(), (VFQuestion) q, 2);
        }
        }
        for(Player player : players1Phase.getPlayers()){
            System.out.println(player.getScore());
        }

        Player[] winners = players1Phase.selectWinners( 3);

        System.out.println("Félicitation aux joueurs ");
        for(Player player : winners){
            System.out.println(player.getName() + "  au score de "+ player.getScore() );
        }


        return winners;

    }

    public Player[] startPhase2(Players players){

        List<Theme> availableThemes =  this.themes.selectNumberThemes(6);
        List<Question> qs = new ArrayList<Question>();

        for(Player player : players.getPlayers()){
            var choosenThemes = gameService.chooseThemes(player, availableThemes);
            System.out.println(choosenThemes.get(0).getName() + "  " + choosenThemes.get(1).getName());
            availableThemes.remove(choosenThemes.get(0));
            availableThemes.remove(choosenThemes.get(1));


            var tql =  this.themeQuestionsList.stream().filter(tq -> (tq.getTheme() == choosenThemes.get(0) ) || (tq.getTheme() == choosenThemes.get(1)) ).collect(Collectors.toList());
            System.out.println(" tql size : " + tql.size());
             qs =  tql.get(0).getQuestions();
             qs.addAll(tql.get(1).getQuestions());
             qs = qs.stream().filter(q  -> q.getDifficultyLevel() == 2).collect(Collectors.toList());
        }

        for(Question q : qs){
            TypeQuestion typeQuestion =  q.getStatement().getTypeQuestion();
            switch (typeQuestion.toString()){
                case "RC" -> gameService.askQuestion(players.getPlayers(), (RCQuestion) q, 3);
                case "QCM" -> gameService.askQuestion(players.getPlayers(), (QCMQuestion) q, 3);
                case "VF" -> gameService.askQuestion(players.getPlayers(), (VFQuestion) q, 3);
            }
        }

        Player[] winners = players.selectWinners( 2);

        System.out.println("Félicitation aux joueurs ");
        for(Player player : winners){
            System.out.println(player.getName() + "  au score de "+ player.getScore() );
        }

        return winners;

    }

    public void startPhase3(Players players){
        List<Theme> availableThemes =  this.themes.selectNumberThemes(3);
        List<Question> qs = new ArrayList<Question>();
        for(Player player : players.getPlayers()){
            var tql =  this.themeQuestionsList.stream().filter(tq -> ((tq.getTheme() == availableThemes.get(0) ) || (tq.getTheme() == availableThemes.get(1)))  ).collect(Collectors.toList());
            qs =  this.themeQuestionsList.get(0).getQuestions();

        }

        for(Question q : qs){
            TypeQuestion typeQuestion =  q.getStatement().getTypeQuestion();
            switch (typeQuestion.toString()){
                case "RC" -> gameService.askQuestion(players.getPlayers(), (RCQuestion) q, 5);
                case "QCM" -> gameService.askQuestion(players.getPlayers(), (QCMQuestion) q, 5);
                case "VF" -> gameService.askQuestion(players.getPlayers(), (VFQuestion) q, 5);
            }
        }

        Player[] winners = players.selectWinners( 1);
    }



    public void initGame(){
        gameService.initThemes();
        gameService.initThemeQuestions();
        gameService.initQuestions();
        gameService.initPlayers();
    }


    public static void main(String[] args){

        ThemeList themeList = new ThemeList();
        ThemeQuestions themeQuestions = new ThemeQuestions();
        Questions questions = new Questions();
        Players players = new Players();


        GameService gameService = new GameService(themeList, questions, players);



        Game game = new Game(gameService);
        game.initGame();
        Player[] winnersofPhase1 = game.startPhase1();
        Player[] winnersofPhase2 = game.startPhase2(new Players(winnersofPhase1));
        game.startPhase3(new Players(winnersofPhase2));


       /* for(ThemeQuestions tq : game.getThemeQuestionsList()){
            System.out.println(tq.show());
        } */
         //System.out.println(game.getPlayers().show());

    }
}
