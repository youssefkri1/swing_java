package com.rattrapageswing.component;

public class VFQuestion extends Question{
    boolean response;

    VFQuestion(Statement statement, Theme theme, int difficultyLevel, boolean response){
        super(statement, theme, difficultyLevel);
        this.response = response;
    }
    @Override
    public String afficherQuestion() {
        return this.statement.getQuestion();
    }

    public boolean saisir(boolean response){
        if(this.response == response){
            return true;
        }
        return false;
    }
}
