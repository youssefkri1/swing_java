package com.rattrapageswing.component;

public enum TypeQuestion {
    QCM,
    VF,
    RC
}
