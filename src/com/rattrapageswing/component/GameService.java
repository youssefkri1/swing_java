package com.rattrapageswing.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GameService {

     Questions questions;
     ThemeList themes;
     ThemeQuestions themeQuestions;
     List<ThemeQuestions> themeQuestionsList;
     Players players;
     Player[] selectedPlayers;

    GameService(ThemeList themes, Questions questions, Players players){
        this.questions = questions;
        this.themes = themes;
        this.players = players;

    }

    public void addRCQuestion(int num_theme, String question, int difficultyLevel, String response){
        Theme theme = this.themes.getThemes().get(num_theme);
        Statement statement = new Statement(question, TypeQuestion.RC);
        RCQuestion rc = new RCQuestion(statement, theme, difficultyLevel, response);
        this.questions.addQuestion(theme, rc);
        System.out.println( this.questions.show());
    }

    public void addVFQuestion(int num_theme, String question, int difficultyLevel, boolean response){
        Theme theme = this.themes.getThemes().get(num_theme);
        Statement statement = new Statement(question, TypeQuestion.VF);
        VFQuestion rc = new VFQuestion(statement, theme, difficultyLevel, response);
        this.questions.addQuestion(theme, rc);
    }

    public void addQCMQuestion(int num_theme, String question, int difficultyLevel,String[] responses,  int response){
        Theme theme = this.themes.getThemes().get(num_theme);
        Statement statement = new Statement(question, TypeQuestion.QCM);
        QCMQuestion rc = new QCMQuestion(statement, theme, difficultyLevel, responses,  response);
        this.questions.addQuestion(theme, rc);
    }

    public void initThemes(){
        this.themes = new ThemeList(true);
    }

    public void initThemeQuestions(){
        Statement statement1 = new Statement("Est-ce que pour surfer, il faut etre un bon nageur ?", TypeQuestion.VF);
        VFQuestion rc1 = new VFQuestion(statement1, this.themes.getThemes().get(0), 1, true);
        System.out.println(rc1.afficherQuestion());

        Statement statement2 = new Statement("Les plus grands matchs de boxe sont organisés à New York?", TypeQuestion.VF);
        VFQuestion vf = new VFQuestion(statement2, this.themes.getThemes().get(0), 1, false);

        Statement statementQCM = new Statement("Qui est le champion actuel de UFC poid léger ?", TypeQuestion.QCM);
        QCMQuestion qcm = new QCMQuestion(statementQCM , this.themes.getThemes().get(0), 2, new String[]{"Dustin Poirier", "Charles Olivera", "Khabib Numagamedov"}, 1);
        System.out.println(qcm.afficherQuestion());

        Statement statement01 = new Statement("A quelle année Mike Tyson a fait son retour au ring (54ans)  ?", TypeQuestion.RC);
        RCQuestion rc01 = new RCQuestion(statement01, this.themes.getThemes().get(0), 2, "2021");

        Statement statement02 = new Statement("Mike Tyson a fait son retour au ring à l'age de 54 ans ?", TypeQuestion.VF);
        VFQuestion rc2 = new VFQuestion(statement02, this.themes.getThemes().get(0), 3, true);

        Statement statement3 = new Statement("le champion de surf qui a représenté le maroc aux jeux olympiques?", TypeQuestion.RC);
        RCQuestion rc3 = new RCQuestion(statement3, this.themes.getThemes().get(0), 3, "boukhiyam");

        Statement statement4 = new Statement("L'année de la fin de la guerre mondiale 1?", TypeQuestion.RC);
        RCQuestion rc4 = new RCQuestion(statement4, this.themes.getThemes().get(1), 1, "1918");

        Statement statement5 = new Statement("Année de la fin de la guerre mondiale 2 ?", TypeQuestion.QCM);
        QCMQuestion rc5 = new QCMQuestion(statement5, this.themes.getThemes().get(1), 1, new String[]{"1918", "1945", "1929"}, 2);

        Statement statement6 = new Statement("Quand a eu lieu la chutte de l'empire romain ?", TypeQuestion.RC);
        RCQuestion rc6 = new RCQuestion(statement6, this.themes.getThemes().get(1), 2, "476");

        Statement statement7 = new Statement("L'empire romain qui a brulé la moitié de Rome ?", TypeQuestion.QCM);
        QCMQuestion rc7 = new QCMQuestion(statement7, this.themes.getThemes().get(1), 2,new String[]{"Neon", "Cesar", "Constentin"}, 0);

        Statement statement03 = new Statement("Il se désigne lui même premier flic de France ?", TypeQuestion.QCM);
        QCMQuestion rc03 = new QCMQuestion(statement03, this.themes.getThemes().get(1),3, new String[]{"Georges Clemenceau", "Napoleon", "Emanuel Macron"}, 0);

        Statement statement8 = new Statement("La révolution francaise a duré de 1789 à 1799 ?", TypeQuestion.VF);
        VFQuestion rc8 = new VFQuestion(statement8, this.themes.getThemes().get(1), 3, true);

        Statement statement9 = new Statement("Qui a dit : « Le sort en est jeté » (Alea jacta est) ?", TypeQuestion.RC);
        RCQuestion rc9 = new RCQuestion(statement9, this.themes.getThemes().get(2), 1, "Cesar");

        Statement statement10 = new Statement("Par quel mot désigne-t-on une belle-mère cruelle ? (maratre)", TypeQuestion.RC);
        RCQuestion rc10 = new RCQuestion(statement10, this.themes.getThemes().get(2), 1, "maratre");

        Statement statement11 = new Statement("Que signifie « palimpseste ?", TypeQuestion.QCM);
        QCMQuestion rc11 = new QCMQuestion(statement11, this.themes.getThemes().get(2), 2,new String[]{ "bouteille magique", "la chambre secrète","Parchemin dont on a effacé la première écriture"}, 2);

        Statement statement12 = new Statement("Asterix et Obelix sont des galois ?", TypeQuestion.VF);
        VFQuestion rc12 = new VFQuestion(statement12, this.themes.getThemes().get(2), 2, true);

        Statement statement13 = new Statement("La plus belle ville de la france ?", TypeQuestion.RC);
        RCQuestion rc13 = new RCQuestion(statement13, this.themes.getThemes().get(2), 3, "nice");

        Statement statement14 = new Statement("La capitale de france?", TypeQuestion.RC);
        RCQuestion rc14 = new RCQuestion(statement14, this.themes.getThemes().get(2), 3, "paris");

        Statement statement15 = new Statement("Einstein est le meilleur physiciste de tous les temps ?", TypeQuestion.VF);
        VFQuestion rc15 = new VFQuestion(statement15, this.themes.getThemes().get(3), 1, true);

        Statement statement16 = new Statement("Qui a découvert la pesanteur ?", TypeQuestion.QCM);
        QCMQuestion rc16 = new QCMQuestion(statement16, this.themes.getThemes().get(3),1, new String[]{ "Pascal", "Newton","Hicham"},1 );

        Statement statement17 = new Statement("Un ordinateur est une machine autonome?", TypeQuestion.VF);
        VFQuestion rc17 = new VFQuestion(statement17, this.themes.getThemes().get(3), 2, false);

        Statement statement18 = new Statement("La première entreprise à commercialiser un portable est motorolla ?", TypeQuestion.VF);
        VFQuestion rc18 = new VFQuestion(statement18, this.themes.getThemes().get(3), 2, true);

        Statement statement19 = new Statement("Les mathématiques science théorique?", TypeQuestion.VF);
        VFQuestion rc19 = new VFQuestion(statement19, this.themes.getThemes().get(3), 3, false);

        Statement statement20 = new Statement("Le centre de la terre est extrèmement chaud ?", TypeQuestion.VF);
        VFQuestion rc20 = new VFQuestion(statement20, this.themes.getThemes().get(3), 3, true);

        Statement statement21 = new Statement("Qui a chanté : I see fire , est-ce Ed-sheeran?", TypeQuestion.VF);
        VFQuestion rc21 = new VFQuestion(statement21, this.themes.getThemes().get(4), 1, true);

        Statement statement22 = new Statement("Ou est-ce que Frank Sinatra est né?", TypeQuestion.RC);
        RCQuestion rc22 = new RCQuestion(statement22, this.themes.getThemes().get(4), 1, "us");

        Statement statement23 = new Statement("Qui a chanté : Layla", TypeQuestion.QCM);
        QCMQuestion rc23 = new QCMQuestion(statement23, this.themes.getThemes().get(4), 2, new String[]{"Gary BB", "Eric Clapton", "Daniel Castro" },1);

        Statement statement24 = new Statement("Qui a chanté The sky is crying ?", TypeQuestion.QCM);
        QCMQuestion rc24 = new QCMQuestion(statement24, this.themes.getThemes().get(4), 2, new String[]{"Gary BB", "BB king", "Daniel Castro" }, 0);

        Statement statement25 = new Statement("groupe de rock connu de Liverpool  ?", TypeQuestion.RC);
        RCQuestion rc25 = new RCQuestion(statement25, this.themes.getThemes().get(4), 3, "the beatles");

        Statement statement26 = new Statement("Qui a chanté : Hotel California?", TypeQuestion.RC);
        RCQuestion rc26 = new RCQuestion(statement26, this.themes.getThemes().get(4), 3, "the eagles");

        Statement statement27 = new Statement("Est-ce que Prison break a été filmé au Maroc", TypeQuestion.VF);
        VFQuestion rc27 = new VFQuestion(statement27, this.themes.getThemes().get(5), 1, true);

        Statement statement28 = new Statement("Intouchable est le film francais le plus connu en france ?", TypeQuestion.VF);
        VFQuestion rc28 = new VFQuestion(statement28, this.themes.getThemes().get(5), 1, false);

        Statement statement29 = new Statement("Est-ce que Gladiator a été filmé à Essaouira ?", TypeQuestion.VF);
        VFQuestion rc29 = new VFQuestion(statement29, this.themes.getThemes().get(5), 2, true);

        Statement statement30 = new Statement("Angelina Jolie a joué dans : ", TypeQuestion.QCM);
        QCMQuestion rc30 = new QCMQuestion(statement30, this.themes.getThemes().get(5), 2, new String[]{"Les misérables", "Les méchants", "60 secondes chrono"}, 2);

        Statement statement31 = new Statement("Qui est le personnage principale de 007?", TypeQuestion.RC);
        RCQuestion rc31 = new RCQuestion(statement31, this.themes.getThemes().get(5), 3, "James Bond");

        Statement statement32 = new Statement(" Quel sèrie a fait le buzz cette année?", TypeQuestion.QCM);
        QCMQuestion rc32 = new QCMQuestion(statement32, this.themes.getThemes().get(5), 3, new String[]{"Peaky Blinders", "Squit Qame", "Black list"},1);

        Statement statement33 = new Statement("Il traverse le lac sans se mouiller?", TypeQuestion.RC);
        RCQuestion rc33 = new RCQuestion(statement33, this.themes.getThemes().get(6), 1, "ombre");

        Statement statement34 = new Statement("Il porte mille vetements (légumes)?", TypeQuestion.RC);
        RCQuestion rc34 = new RCQuestion(statement34, this.themes.getThemes().get(6), 1, "oignon");

        Statement statement35 = new Statement(" Le vin en fruit ?", TypeQuestion.RC);
        RCQuestion rc35 = new RCQuestion(statement35, this.themes.getThemes().get(6), 2, "raisin");

        Statement statement36 = new Statement("Elle s'allonge et se  rétrécit en même temps?", TypeQuestion.RC);
        RCQuestion rc36 = new RCQuestion(statement36, this.themes.getThemes().get(6), 2, "la vie");

        Statement statement37 = new Statement("Quand je suis frais, je suis chaud ?", TypeQuestion.RC);
        RCQuestion rc37 = new RCQuestion(statement37, this.themes.getThemes().get(6), 3, "pain");

        Statement statement38 = new Statement("Je transforme une plante en une planète?", TypeQuestion.QCM);
        QCMQuestion rc38 = new QCMQuestion(statement38, this.themes.getThemes().get(6), 3, new String[]{"é", "c", "d"}, 0);

        Statement statement39 = new Statement("La capitale de france ?", TypeQuestion.RC);
        RCQuestion rc39 = new RCQuestion(statement39, this.themes.getThemes().get(7), 1, "paris");

        Statement statement40 = new Statement("La capitale d'Espagne ?", TypeQuestion.RC);
        RCQuestion rc40 = new RCQuestion(statement40, this.themes.getThemes().get(7), 1, "madrid");

        Statement statement41 = new Statement("La capitale de Danmark ?", TypeQuestion.RC);
        RCQuestion rc41 = new RCQuestion(statement41, this.themes.getThemes().get(7), 2, "copenhagn");

        Statement statement42 = new Statement("La capitale de swiss ?", TypeQuestion.RC);
        RCQuestion rc42 = new RCQuestion(statement42, this.themes.getThemes().get(7), 2, "genève");

        Statement statement43 = new Statement("La capitale de l'angleterre ?", TypeQuestion.QCM);
        QCMQuestion rc43 = new QCMQuestion(statement43, this.themes.getThemes().get(7), 3, new String[]{"londre", "rabat", "bermingham"}, 1);

        Statement statement44 = new Statement("La capitale de Dagistan ?", TypeQuestion.QCM);
        QCMQuestion rc44 = new QCMQuestion(statement44, this.themes.getThemes().get(7), 3, new String[]{"londre", "rabat", "makhachkala"}, 2);

        Statement statement45 = new Statement("Do you speak english ?", TypeQuestion.RC);
        VFQuestion rc45 = new VFQuestion(statement45, this.themes.getThemes().get(8), 1, true);

        Statement statement46 = new Statement("What programming language this exam is about ?", TypeQuestion.RC);
        RCQuestion rc46 = new RCQuestion(statement46, this.themes.getThemes().get(8), 1, "java");

        Statement statement47 = new Statement("Is Swing a library of java ?", TypeQuestion.RC);
        VFQuestion rc47 = new VFQuestion(statement47, this.themes.getThemes().get(8), 2, true);

        Statement statement48 = new Statement("In which country i was sent to excchange studies?", TypeQuestion.QCM);
        QCMQuestion rc48 = new QCMQuestion(statement48, this.themes.getThemes().get(8), 2,new String[]{"England", "Brazil", "Russia"}, 0);

        Statement statement49 = new Statement("what .. you usually eat for breakfast  ? ", TypeQuestion.QCM);
        QCMQuestion rc49 = new QCMQuestion(statement49, this.themes.getThemes().get(8), 3, new String[]{"do", "will", "were"}, 0);

        Statement statement50 = new Statement(".... are you going ? -> I am going to work ?", TypeQuestion.QCM);
        QCMQuestion rc50 = new QCMQuestion(statement50, this.themes.getThemes().get(8), 3, new String[]{"where", "what", "why"}, 0);

        Statement statement51 = new Statement("2+2=4 ?", TypeQuestion.VF);
        VFQuestion rc51 = new VFQuestion(statement51, this.themes.getThemes().get(9), 1, true);

        Statement statement52 = new Statement("4*4=17 ?", TypeQuestion.VF);
        VFQuestion rc52 = new VFQuestion(statement52, this.themes.getThemes().get(9), 1, false);

        Statement statement53 = new Statement("6*(6-(3*2)) = 0 ?", TypeQuestion.VF);
        VFQuestion rc53 = new VFQuestion(statement53, this.themes.getThemes().get(9), 2, true);

        Statement statement54 = new Statement("racine carré de 49 ?", TypeQuestion.RC);
        RCQuestion rc54 = new RCQuestion(statement54, this.themes.getThemes().get(9), 2, "7");

        Statement statement55 = new Statement("Racine carré de 64?", TypeQuestion.RC);
        RCQuestion rc55 = new RCQuestion(statement55, this.themes.getThemes().get(9), 3, "8");

        Statement statement56 = new Statement("10 au carré?", TypeQuestion.RC);
        RCQuestion rc56 = new RCQuestion(statement56, this.themes.getThemes().get(9), 3, "100");


        List<Question> lqs0 = new ArrayList<Question>(Arrays.asList(rc1,
                vf,
                qcm,
                rc01,
                rc2,
                rc3));
        ThemeQuestions themeQuestions0 = new ThemeQuestions(this.themes.getThemes().get(0), lqs0);

        List<Question> lqs1 = new ArrayList<Question>(Arrays.asList(rc4,
                rc5,
                rc6,
                rc7,
                rc03,
                rc8));
        ThemeQuestions themeQuestions1 = new ThemeQuestions(this.themes.getThemes().get(1), lqs1);

        List<Question> lqs2 = new ArrayList<Question>(Arrays.asList(rc9,
                rc10,
                rc11,
                rc12,
                rc13,
                rc14));
        ThemeQuestions themeQuestions2 = new ThemeQuestions(this.themes.getThemes().get(2), lqs2);

        List<Question> lqs3 = new ArrayList<Question>(Arrays.asList(rc15,
                rc16,
                rc17,
                rc18,
                rc19,
                rc20));
        ThemeQuestions themeQuestions3 = new ThemeQuestions(this.themes.getThemes().get(3), lqs3);

        List<Question> lqs4 = new ArrayList<Question>(Arrays.asList(rc21,
                rc22,
                rc23,
                rc24,
                rc25,
                rc26));
        ThemeQuestions themeQuestions4 = new ThemeQuestions(this.themes.getThemes().get(4), lqs4);

        List<Question> lqs5 = new ArrayList<Question>(Arrays.asList(rc27,
                rc28,
                rc29,
                rc30,
                rc31,
                rc32));
        ThemeQuestions themeQuestions5 = new ThemeQuestions(this.themes.getThemes().get(5), lqs5);

        List<Question> lqs6 = new ArrayList<Question>(Arrays.asList(rc33,
                rc34,
                rc35,
                rc36,
                rc37,
                rc38));
        ThemeQuestions themeQuestions6 = new ThemeQuestions(this.themes.getThemes().get(6), lqs6);

        List<Question> lqs7 = new ArrayList<Question>(Arrays.asList(rc39,
                rc40,
                rc41,
                rc42,
                rc43,
                rc44));
        ThemeQuestions themeQuestions7 = new ThemeQuestions(this.themes.getThemes().get(7), lqs7);

        List<Question> lqs8 = new ArrayList<Question>(Arrays.asList(rc45,
                rc46,
                rc47,
                rc48,
                rc49,
                rc50));
        ThemeQuestions themeQuestions8 = new ThemeQuestions(this.themes.getThemes().get(8), lqs8);

        List<Question> lqs9 = new ArrayList<Question>(Arrays.asList(rc51,
                rc52,
                rc53,
                rc54,
                rc55,
                rc56));
        ThemeQuestions themeQuestions9 = new ThemeQuestions(this.themes.getThemes().get(9), lqs9);

        this.themeQuestionsList = new ArrayList<ThemeQuestions>(Arrays.asList(themeQuestions0, themeQuestions1, themeQuestions2, themeQuestions3, themeQuestions4, themeQuestions5, themeQuestions6, themeQuestions7, themeQuestions8, themeQuestions9));
    }

    public void initQuestions(){
        System.out.println(this.themeQuestionsList.size());
        this.questions = new Questions(this.themeQuestionsList);
    }

    public void initPlayers(){
        this.players = new Players(true);
    }

    public void askQuestion(Player[] players, RCQuestion question, int point){

        for(Player player: players){
            System.out.println("Question pour Player " + player.getName() );
            System.out.println(question.afficherQuestion() +"\n");
            Scanner scanner = new Scanner( System.in );
            String rc = scanner.nextLine();
            player.saisir(question, rc, point);
            System.out.println("Le joueur " + player.getName() + "a " +  player.getScore() + "points .");
        }


    }

    public void askQuestion(Player[] players, VFQuestion question, int point){

        for(Player player: players){
            System.out.println("Question pour Player " + player.getName() + "\n");
            System.out.println(question.afficherQuestion() +"\n");
            Scanner scanner = new Scanner( System.in );
            boolean bn = scanner.nextBoolean();
            player.saisir(question, bn, point);
            System.out.println("Le joueur " + player.getName() + "a " +  player.getScore() + "points .");
        }

    }

    public void askQuestion(Player[] players, QCMQuestion question, int point){

        for(Player player: players){
            System.out.println("Question pour Player " + player.getName() + "\n");
            System.out.println(question.afficherQuestion() +"\n");
            System.out.println("Choisissez entre le 1, le 2 ou le 3");
            Scanner scanner = new Scanner( System.in );
            int rep = scanner.nextInt();
            player.saisir(question, rep, point);
            System.out.println("Le joueur " + player.getName() + "a " +  player.getScore() + "points .");
        }

    }

    public List<Theme> chooseThemes(Player player, List<Theme> themes){

        System.out.println("Choisis 2 entre ses thèmes :");
        for(int i = 1; i< themes.size()+1; i++){
            System.out.println( i +  "ème thème : " + themes.get(i-1).getName());
        }
        Scanner scanner = new Scanner( System.in );
        int premierTheme = scanner.nextInt()-1;
        int deuxièmeTheme = scanner.nextInt()-1;

        return new ArrayList<Theme>(Arrays.asList(themes.get(premierTheme), themes.get(deuxièmeTheme)));
    }

    public List<ThemeQuestions> getThemeQuestionsList() {
        return themeQuestionsList;
    }

    public void initGame(){
        this.initThemes();
        this.initThemeQuestions();
        this.initQuestions();
        this.initPlayers();
    }

    public Player[] selectNmbPlayers(int nb){
        Player[] selectedPlayers = this.players.selectNmbPlayers(nb);
        for(Player player : selectedPlayers){
            if(Arrays.stream(selectedPlayers).toList().contains(player)){
                player.setState(PlayerState.SELECTED);
            }
        }
        return selectedPlayers;
    }

    public Player[] selectWinners(Player[] selectedPlayers, int nbWinners){
        Player min = selectedPlayers[0];
        int indiceMin = 0;
        Player[] result = new Player[nbWinners];
        for(int i=1; i<selectedPlayers.length; i++) {
            if (selectedPlayers[i].getScore() < min.getScore()) {
                min = selectedPlayers[i];
                indiceMin = i;
            }
        }

        int j =0;
        for(int i = 0; i<selectedPlayers.length; i++){
            if(i != indiceMin){
                result[j] = selectedPlayers[i];
                j++;
            }
        }
        return result;
    }




}
