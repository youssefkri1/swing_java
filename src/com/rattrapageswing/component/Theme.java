package com.rattrapageswing.component;

public class Theme {
    private String name;
    private boolean isSelected = false;

    Theme(String name){

        this.name = name;
        this.isSelected = false;
    }
    Theme(){

    }

    public boolean getIsSelected(){
        return this.isSelected;
    }

    public void setIsSelected(boolean isSelected){
        this.isSelected = isSelected;
    }

    private void modifyTheme(){

    }

    public String getName() {
        return name;
    }

}
